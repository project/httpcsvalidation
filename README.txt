INTRODUCTION
------------
This plugin helps you to do the HTTPCS validation process automatically.
It will contact the HTTPCS validator service to get a validation file which
will help us to validate your website automatically. Also, you will be able
to signup at HTTPCS.com if you don't have an account yet or to login if you
already own an HTTPCS acccount.

REQUIREMENTS
------------
No special requirements

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration.
Enabling or disabling the module won't affect your website except your admin
menu. You will be able to see 'HTTPCS Validation' in the configuration section.
