<?php

/**
 * @file
 * Httpcs configuration.
 */

$configArray                         = new stdClass();
$configArray->PATHHTTPCS             = 'https://www.httpcs.com';
$configArray->PATHCHECKURLEMAIL      = 'https://www.httpcs.com/plugincms/checkurlemail';
$configArray->PATHCHECKEMAILPASSWORD = 'https://www.httpcs.com/plugincms/checkemailpassword';
$configArray->PATHCHECKFILE          = 'https://www.httpcs.com/plugincms/checkfile';
$configArray->PATHCHECKFILEAGAIN     = 'https://www.httpcs.com/plugincms/checkfileagain';
